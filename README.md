# README #

Virtual Reality - Ein Markt der gerade erst entsteht. Diese Website ist im Rahmen eines Schulprojektes entstanden und zeigt was Virtuelle Realität ist und wie sie unser Leben verändern könnte


### How do I get set up? ###

* Just clone this repo and you're setup
* Alternatively you can just implement this website into your own website by copying the following code into your body:
<iframe src="https://lukasstrutz.de/website" style="border:none;" title="VR Website" width="100%" height="100%"></iframe>


### Who do I talk to? ###

* If you want to contact me, just send me an E-Mail at lukas@lukasstrutz.de or send me a message via Discord: AntonMC#2387